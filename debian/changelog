python-flask-cors (5.0.0-1) unstable; urgency=medium

  * Team upload.
  * d/control: Use correct syntax for build profiles
  * New upstream version 5.0.0
  * Rebuild patch queue from patch-queue branch
    Added patch:
    docs-Fixup-small-typo-on-reponse.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 14 Sep 2024 09:41:57 +0200

python-flask-cors (4.0.1-1) unstable; urgency=medium

  * Team upload.

  [ Alexandre Detiste ]
  * remove dependency on old python3-six

  [ Carsten Schoenert ]
  * d/watch: Add compression=gz to opts
  * New upstream version 4.0.1
    Fixes CVE-2024-1681
    (Closes: 1069764)
  * Rebuild patch queue from patch-queue branch
    Added patch:
    upstream/tests-Use-importlib.metadata.version-flask.patch
    Updated patches:
    debian-hacks/Privacy-Remove-linking-to-external-resources.patch
    debian-hacks/README-Link-to-internal-HTML-resource.patch
    debian-hacks/docs-Use-local-inventory-for-Python3.patch
  * d/control: Increase Standards-Version to 4.7.0
    No further modifications needed.
  * d/copyright: Update year data
  * d/rules: Add target override_dh_clean
    Clean up one more possible folder.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 10 May 2024 20:52:32 +0200

python-flask-cors (4.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.0.0
  * Rebuild patch queue from patch-queue branch
    Updated patches:
    debian-hacks/Privacy-Remove-linking-to-external-resources.patch
    debian-hacks/README-Link-to-internal-HTML-resource.patch
    Dropped patches (fixed upstream):
    upstream/Spelling-Fix-misspelled-word-conjuction.patch
    upstream/Spelling-Fix-misspelled-word-maching.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 27 Jun 2023 17:15:37 +0200

python-flask-cors (3.0.10-2) unstable; urgency=medium

  * Team upload.
  * d/watch: Use git mode to check newer versions
  * d/control: Increase Standards-Version to 4.6.2
    No further modifications needed.
  * d/control: Adjust/update B-D, add BuildProfileSpecs
  * d/rules: Remove --with option from default target
  * d/copyright: Adjust Homepage to GH project space
  * d/u/metadata: Adjust some more data
  * d/copyright: Update year data
  * autopkgtest: Be more specific on depending packages

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 22 Jan 2023 09:52:05 +0100

python-flask-cors (3.0.10-1) unstable; urgency=medium

  * Team upload.
  * d/gbp.conf: Add more settings
  * New upstream version 3.0.10
  * Rebuild patch queue from patch-queue branch
    Renamed patches:
    remove_badges_from_doc
     -> debian-hacks/Privacy-Remove-linking-to-external-resources.patch
    redirect_api_links_locally
     -> debian-hacks/README-Link-to-internal-HTML-resource.patch
    Added patches:
    debian-hacks/docs-Use-local-inventory-for-Python3.patch
    upstream/Spelling-Fix-misspelled-word-conjuction.patch
    upstream/Spelling-Fix-misspelled-word-maching.patch
    Dropped patch (fixed upstream):
    spelling_error_in_manpage
  * d/control: Make DPT to the package maintainer
  * d/{control,rules}: Move over to dh-sequence-python3
  * d/control: Add new entries, sort Build-Depends alphabetical
    (Closes: #1018495)
  * d/u/metadata: Fix small typo
  * autopkgtest: Switch to use pytest instead of nose
  * d/rules: Use pytest to run internal tests
  * documentation: Build HTML and manpage by dh_sphinxdoc
  * d/control: Increase Standards-Version to 4.6.1
    No further modifications needed.
  * d/salsa-ci.yml: Adding Yaml CI control data for Salsa
    Rename d/.gitlab-ci.yml to d/salsa-ci.yml, update the content of the
    file.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 01 Nov 2022 08:15:36 +0100

python-flask-cors (3.0.9-2) unstable; urgency=medium

  * Team upload.

  [ Bastian Germann ]
  * d/copyright: Fix superfluous license.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 18 Dec 2020 11:06:56 -0500

python-flask-cors (3.0.9-1) unstable; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * d/gbp.conf: use team's branch names and migrate to debian/master.
  * d/control: upgrade to dh13.
  * d/control: update Standards-Version to 4.5.1. Add Rules-Requires-Root.
  * d/control: the team is not called the Python Team.
  * d/tests: add autopkgtest.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Bastian Germann ]
  * Add gbp.conf
  * New upstream version 3.0.9 (Closes: #950058, #969362)

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 18 Dec 2020 10:54:57 -0500

python-flask-cors (3.0.8-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

  [ Stewart Ferguson ]
  * Bumping version to facilitate source-only upload

 -- Stewart Ferguson <stew@ferg.aero>  Tue, 30 Jul 2019 19:11:58 +0200

python-flask-cors (3.0.8-1) unstable; urgency=medium

  * Upstream release 3.0.8
  * Bumping standards-version 4.2.1 -> 4.3.0 (no changes required)
  * Bumping compat 11 -> 12 and replacing d/compat with newer build-dep
  * Adding d/upstream/metadata

 -- Stewart Ferguson <stew@ferg.aero>  Sun, 09 Jun 2019 09:29:19 +0200

python-flask-cors (3.0.7-1) unstable; urgency=medium

  * Initial release (Closes: #915789)

 -- Stewart Ferguson <stew@ferg.aero>  Wed, 05 Dec 2018 21:51:05 +0100
